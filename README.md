## Details

A collection of `zsh` scripts useful for getting various bits of information from guest virtual machines that are running under KVM/QEMU.

For information on each script, please see the `README.md` within each script's folder:

- [`guest-disk-usage`](./guest-disk-usage/README.md)
